// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  tokenKey: 'token',
  userData: 'user',
  firebase: {
    apiKey: "AIzaSyDuQZ6495uL8HVrF9DigP6wNlbh2FIFYvo",
    authDomain: "notebook-43501.firebaseapp.com",
    projectId: "notebook-43501",
    storageBucket: "notebook-43501.appspot.com",
    messagingSenderId: "1062292115636",
    appId: "1:1062292115636:web:f88d2bcc7e1c4df82a47e1",
    measurementId: "G-26CDZKCW1N"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
