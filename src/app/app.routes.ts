import { RouterModule, Routes } from "@angular/router";

import { GoogleAuthGuard } from "./auth/google.guard";
import { HomeComponent } from "./home/home.component";
import { LoginComponent } from "./login/login.component";
import { NavComponent } from "./nav/nav.component";
import { SubjectComponent } from "./subject/subject.component";

const appRoutes: Routes = [

    { path: 'login', component: LoginComponent},
    {
        path: 'subjects',
        component: SubjectComponent,
        canActivate: [GoogleAuthGuard]
    },
    { path: '', component: NavComponent, canActivate: [GoogleAuthGuard],  children: [

        { path: 'home', component: HomeComponent, canActivate: [GoogleAuthGuard]},
        {
            path: 'topics',
            loadChildren: () => import('./topics/topics.module').then(m => m.TopicsModule)
        },
        {
            path: 'content',
            loadChildren: () => import('./content/content.module').then(m => m.ContentModule)
        }

    ]}
]

export const APP_ROUTES = RouterModule.forRoot(appRoutes, { scrollPositionRestoration: 'enabled' });
