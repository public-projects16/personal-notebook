import { AngularFireAuth } from '@angular/fire/auth';
import { Injectable } from "@angular/core";
import { Observable, from } from "rxjs";
import * as _ from 'lodash';
import fbase  from 'firebase/app';
import firebase from "firebase";

import { environment } from "src/environments/environment";

@Injectable({
    providedIn: 'root'
  })
  export class AuthService {
    

    constructor(private fireAuth: AngularFireAuth) { 
        if (_.isEmpty(firebase.apps)) {
            firebase.initializeApp(environment.firebase);
        }  
    }

    public loginViaGoogle(): Observable<fbase.auth.UserCredential> {
        return from(this.fireAuth.signInWithPopup(new fbase.auth.GoogleAuthProvider()));
    }
    
    logout(): Observable<void> {
        localStorage.clear()
        return from(this.fireAuth.signOut());
    }

    public getToken() {
        return localStorage.getItem(environment.tokenKey);
    }

    public getUserData(): firebase.User {
        const userData = localStorage.getItem(environment.userData);
        return JSON.parse(userData || '') as any;
    }

    public setUserId(value: string) {
        localStorage.setItem('userId', value);
    }

    public getUserId(): string {
        return localStorage.getItem('userId') || '';
    }
  
}