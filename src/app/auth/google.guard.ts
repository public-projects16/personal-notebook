import { ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot } from "@angular/router";
import { Injectable } from "@angular/core";
import firebase from "firebase";
import * as _ from "lodash";

import { environment } from "src/environments/environment";
import { AuthService } from "./auth.service";

@Injectable()
export class GoogleAuthGuard implements CanActivate {

    constructor(private router: Router, private authService: AuthService) {
        if (firebase.apps.length === 0) {
            firebase.initializeApp(environment.firebase);
        }        
    }

    canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        firebase.auth().onAuthStateChanged((userFound) => {
            if (!userFound) {
                this.router.navigate(['login']);
            }
        });
        
        if ( _.isUndefined(this.authService.getUserId)) {
            this.router.navigate(['login']);
        }
        return true;
    }

}