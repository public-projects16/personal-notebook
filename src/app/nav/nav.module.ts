import { CommonModule } from '@angular/common';
import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NavComponent } from './nav.component';
import { RouterModule, Routes } from '@angular/router';

import { GoogleAuthGuard } from "../auth/google.guard";
import { LoginComponent } from '../login/login.component';
import { LoginModule } from '../login/login.module';
import { MaterialModule } from '../material.module';
import { TopicsModule } from '../topics/topics.module';

const routes: Routes = [
  { path: '', component: NavComponent},
  { path: 'login', component: LoginComponent}
]
@NgModule({
  declarations: [NavComponent],
  imports: [
    CommonModule,
    FormsModule,
    RouterModule.forChild(routes),
    LoginModule,
    TopicsModule,
    MaterialModule,
    ReactiveFormsModule
  ],
  schemas: [ CUSTOM_ELEMENTS_SCHEMA],
  exports: [ FormsModule, ReactiveFormsModule, MaterialModule],
  providers: [GoogleAuthGuard]
})
export class NavModule { }
