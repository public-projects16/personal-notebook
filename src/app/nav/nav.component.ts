import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import * as _ from 'lodash';

import { AuthService } from '../auth/auth.service';
import { Subject } from '../subject/subject';
import { SubjectsService } from '../subject/subjects.service';
import { views } from './views';


@Component({
  selector: 'app-nav',
  templateUrl: './nav.component.html',
  styleUrls: ['./nav.component.css']
})
export class NavComponent implements OnInit {

  public items = views;
  public name: string | null;
  public image: string | null;
  public subjects: Subject[];
  public selectedSubject: Subject;

  constructor(
    private router: Router,
    private authService: AuthService,
    private subjectsService: SubjectsService
  ) {
    this.name = '';
    this.image = '';
    this.subjects = [];
    this.selectedSubject = new Subject();
   }

  async ngOnInit(): Promise<void> {
    this.name = this.authService.getUserData().displayName;
    this.image = this.authService.getUserData().photoURL;
    this.selectedSubject = this.subjectsService.currentSubject;
    this.authService.getToken();
    await this.getSubjects();
  }

  public navigate(route: string): void {
    this.router.navigate([route]);
  }

  public logout() {
    this.authService.logout();
  }

  public async getSubjects() {
    this.subjects = await this.subjectsService.getSubjects();
    const currentSubject =  this.subjectsService.currentSubject;
    const repeatedSubject = _.findIndex(this.subjects, (subject: Subject) => { return _.isEqual(subject.id, currentSubject.id)});
    this.subjects.splice(repeatedSubject, 1);
  }

  public changeSubject(choosenSubject: Subject) {
    this.subjectsService.currentSubject = choosenSubject;
    this.selectedSubject = choosenSubject;
    this.router.routeReuseStrategy.shouldReuseRoute = () => false;
    this.router.onSameUrlNavigation = 'reload';
    this.router.navigate(['topics']);
  }

}
