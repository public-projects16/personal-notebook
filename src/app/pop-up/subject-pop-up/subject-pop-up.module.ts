import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SubjectPopUpComponent } from './subject-pop-up.component';



@NgModule({
  declarations: [SubjectPopUpComponent],
  imports: [
    CommonModule
  ]
})
export class SubjectPopUpModule { }
