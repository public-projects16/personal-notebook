import { TestBed } from '@angular/core/testing';

import { SubjectPopUpService } from './subject-pop-up.service';

describe('SubjectPopUpService', () => {
  let service: SubjectPopUpService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(SubjectPopUpService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
