import { Injectable } from '@angular/core';
import firebase from "firebase";

import { FirebaseCollections } from 'src/app/utils/firebase.collections';
import { Subject } from 'src/app/subject/subject';

@Injectable({
  providedIn: 'root'
})
export class SubjectPopUpService {

  private firestore: firebase.firestore.Firestore;

  constructor() {
    this.firestore = firebase.firestore();
   }

  public createSubject(subject: Subject): Promise<void> {
    let promise: Promise<void>;

    promise = new Promise((resolve, reject) => {
        this.firestore.collection(FirebaseCollections.subject).doc().set(subject.toJSON())
        .then(() => {
            resolve();
        })
        .catch((error: Error) => {
            reject(error);
        });
    });
    return promise;
  }
}
