import { Component, Inject, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup } from '@angular/forms';
import { MatDialog, MAT_DIALOG_DATA } from '@angular/material/dialog';

import { AuthService } from 'src/app/auth/auth.service';
import { NotificationsService } from 'src/app/services/notification.service';
import { Subject } from 'src/app/subject/subject';
import { SubjectPopUpService } from './subject-pop-up.service';

@Component({
  selector: 'app-subject-pop-up',
  templateUrl: './subject-pop-up.component.html',
  styleUrls: ['./subject-pop-up.component.css']
})
export class SubjectPopUpComponent implements OnInit {

  public subjectForm: FormGroup;

  constructor(
    @Inject(MAT_DIALOG_DATA) public dialogData: any,
    private formbuilder: FormBuilder,
    private matDialog: MatDialog,
    private subjectPopUpService: SubjectPopUpService,
    private authService: AuthService,
    private notification: NotificationsService
  ) {
    this.subjectForm = this.formbuilder.group({});
   }

  ngOnInit(): void {
    this.initializeForm();
  }

  public initializeForm(): void {
    this.subjectForm = this.formbuilder.group({
      name: new FormControl('')
    });
  }
  
  public closeDialog(): void {
    this.matDialog.closeAll();
  }

  public createSubject(): void {
    const subject = new Subject();
    subject.name = this.subjectForm.get("name")?.value;
    subject.userId = this.authService.getUserId();
    this.subjectPopUpService.createSubject(subject)
    .then(() => {
      this.notification.success("Materia creada con éxito");
    })
    .catch(() => {
      this.notification.error("Error al crear la materia");
    })
    .finally(() => {
      this.closeDialog();
    })
  }

}
