import { Injectable } from '@angular/core';
import firebase from "firebase";

import { FirebaseCollections } from 'src/app/utils/firebase.collections';
import { Topic } from '../../topics/topic';

@Injectable({
  providedIn: 'root'
})
export class TopicPopUpService {

  private firestore: firebase.firestore.Firestore;

  constructor() {
    this.firestore = firebase.firestore();
   }

  public createTopic(topic: Topic): Promise<void> {
    let promise: Promise<void>;

    promise = new Promise((resolve, reject) => {
        this.firestore.collection(FirebaseCollections.topic).doc().set(topic.toJSON())
        .then(() => {
            resolve();
        })
        .catch((error: Error) => {
            reject(error);
        });
    });
    return promise;
  }
}
