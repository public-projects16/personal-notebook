import { Component, Inject, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup } from '@angular/forms';
import { MatDialog, MAT_DIALOG_DATA } from '@angular/material/dialog';

import { NotificationsService } from 'src/app/services/notification.service';
import { SubjectsService } from 'src/app/subject/subjects.service';
import { Topic } from '../../topics/topic';
import { TopicPopUpService } from './topic-pop-up.service';

@Component({
  selector: 'app-topic-pop-up',
  templateUrl: './topic-pop-up.component.html',
  styleUrls: ['./topic-pop-up.component.css']
})
export class TopicPopUpComponent implements OnInit {

  public topicForm: FormGroup;

  constructor(
    @Inject(MAT_DIALOG_DATA) public dialogData: any,
    private formbuilder: FormBuilder,
    private matDialog: MatDialog,
    private topicPopUpService: TopicPopUpService,
    private subjectService: SubjectsService,
    private notification: NotificationsService
  ) {
    this.topicForm = this.formbuilder.group({});
   }

  ngOnInit(): void {
    this.initializeForm();
  }

  public initializeForm(): void {
    this.topicForm = this.formbuilder.group({
      name: new FormControl('')
    });
  }

  public closeDialog(): void {
    this.matDialog.closeAll();
  }

  public createTopic(): void {
    const topic = new Topic();
    topic.name = this.topicForm.get("name")?.value;
    topic.subjectId = this.subjectService.currentSubject.id;
    this.topicPopUpService.createTopic(topic)
    .then(() => {
      this.notification.success("Tema creado con éxito");
    })
    .catch(() => {
      this.notification.error("Error al crear el tema");
    })
    .finally(() => {
      this.closeDialog();
    })
  }


}
