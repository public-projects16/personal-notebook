import { TestBed } from '@angular/core/testing';

import { TopicPopUpService } from './topic-pop-up.service';

describe('TopicPopUpService', () => {
  let service: TopicPopUpService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(TopicPopUpService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
