import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TopicPopUpComponent } from './topic-pop-up.component';

describe('TopicPopUpComponent', () => {
  let component: TopicPopUpComponent;
  let fixture: ComponentFixture<TopicPopUpComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ TopicPopUpComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(TopicPopUpComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
