import { Component, OnInit } from '@angular/core';
import * as _ from "lodash";

import { AuthService } from '../auth/auth.service';
import { errorMessages } from 'src/app/utils/error.messages';
import { NotificationsService } from '../services/notification.service';
import { Router } from '@angular/router';
import { User } from './user';
import { UserService } from './user.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  public firebaseUser: any;

  constructor(private userService: UserService,
      private authService: AuthService,
      private notification: NotificationsService,
      private router: Router) {
   }

  async ngOnInit(): Promise<void> {
    console.log('Home');
    this.firebaseUser = this.authService.getUserData();
    await this.validateUserService();
    this.router.navigate(['topics'])
  }

  public async validateUserService() {
    try {
      let userFound: User | undefined = _.head(await this.userService.getUserByEmail(this.firebaseUser.email || ''));
      
      if (!userFound) {
        const newUser: User = new User();
        newUser.email = this.firebaseUser.email || '';
        newUser.name = this.firebaseUser.displayName || '';
        await this.userService.createUser(newUser);
        userFound =  _.head(await this.userService.getUserByEmail(this.firebaseUser.email || ''));
      }

      this.authService.setUserId(userFound?.id || '');

    } catch(error) {
      this.notification.error(errorMessages.user.errorCreating);
      this.router.navigate(['login']);
    }
  }

}
