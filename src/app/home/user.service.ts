import { Injectable } from "@angular/core";
import * as _ from 'lodash';
import firebase from "firebase";

import { errorMessages } from "src/app/utils/error.messages";
import { FirebaseCollections } from "src/app/utils/firebase.collections";
import { User } from "./user";

@Injectable()
export class UserService {

    private firestore: firebase.firestore.Firestore;

    constructor() {
        this.firestore = firebase.firestore();
    }

    public getUserByEmail(email: string): Promise<User[]> {
        let subjectsPromise: Promise<User[]>;
        subjectsPromise = new Promise((resolve, reject) => {
            const usersFound: User[] = [];
            this.firestore.collection(FirebaseCollections.user)
            .where("email", "==", email).get()
            .then((querySnapshot: firebase.firestore.QuerySnapshot<firebase.firestore.DocumentData>) => {
                for (const doc of querySnapshot.docs) {
                    const userFound: User = new User();
                    userFound.load(doc.data());
                    userFound.id = doc.id;
                    usersFound.push(userFound);
                }
                resolve(usersFound);
            })
            .catch(() => {
                reject(new Error().message = errorMessages.user.errorCreating);
            })
        });
        return subjectsPromise;
    }

    public createUser(user: User): Promise<void> {
        let promise: Promise<void>;

        promise = new Promise((resolve, reject) => {
            this.firestore.collection(FirebaseCollections.user).doc().set(user.toJSON())
            .then(() => {
                resolve();
            })
            .catch((error: Error) => {
                reject(error);
            });
        });
        return promise;
    }

}