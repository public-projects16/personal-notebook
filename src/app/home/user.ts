import * as _ from "lodash";

export class User {

    private _name: string;
    private _id: string;
    private _email: string;

    constructor() {
        this._name = '';
        this._id = '';
        this._email =  '';
    }

    get name(): string { return this._name; }

    set name(value: string) { this._name = value; }

    get id(): string { return this._id; }

    set id(value: string) { this._id = value; }

    get email(): string { return this._email; }

    set email(value: string) { this._email = value; }

    public load(data: any, prefix: string = "") {
        for (let parameter in this) {
            if (!_.isUndefined(typeof data[prefix + _.camelCase(parameter)]) && !_.isEqual(parameter, "constructor")) {
                this[parameter] = data[prefix + _.camelCase(parameter)];
            }
        }
    }

    public toJSON() {
        return {
            name: this.name,
            email: this.email
        }
    }
}