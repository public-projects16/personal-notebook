import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';

import { NotificationsService } from '../services/notification.service';
import { SubjectPopUpConfiguration } from '../subject/pop-up-subject.config';
import { TopicPopUpComponent } from '../pop-up/topic-pop-up/topic-pop-up.component';
import { TopicsService } from './topics.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-topics',
  templateUrl: './topics.component.html',
  styleUrls: ['./topics.component.css']
})
export class TopicsComponent implements OnInit {

  public topics: any;

  constructor(
    private topicsService: TopicsService,
    private notification: NotificationsService,
    private popUpDialog: MatDialog,
    private router: Router
  ) { }

  async ngOnInit(): Promise<void> {
    await this.setTopics();    
  }

  public async setTopics(): Promise<void> {
    this.topics = await this.topicsService.getTopics();
  }

  public goToTopic(topicId: string) {
    this.router.navigate(['content'], { queryParams: { id: topicId} });
  }

  public createNewTopic() {
    const topicPopUpConfiguration = SubjectPopUpConfiguration.getPopUpConfiguration('Agregar tema', 'add');
    this.popUpDialog.open(TopicPopUpComponent, topicPopUpConfiguration)
    .afterClosed().toPromise().then(() => { this.setTopics(); });
  }
}
