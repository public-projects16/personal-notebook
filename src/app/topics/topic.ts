import * as _ from "lodash";

export class Topic {

    
    private _name: string;
    private _id: string;
    private _subjectId: string;

    constructor() {
        this._name = '';
        this._id = '';
        this._subjectId =  '';
    }

    get name(): string { return this._name; }

    set name(value: string) { this._name = value; }

    get id(): string { return this._id; }

    set id(value: string) { this._id = value; }

    get subjectId(): string { return this._subjectId; }

    set subjectId(value: string) { this._subjectId = value; }

    public load(data: any, prefix: string = "") {
        for (let parameter in this) {
            if (!_.isUndefined(typeof data[prefix + _.camelCase(parameter)]) && !_.isEqual(parameter, "constructor")) {
                this[parameter] = data[prefix + _.camelCase(parameter)];
            }
        }
    }

    public toJSON() {
        return {
            name: this.name,
            subjectId: this.subjectId,
            id: this.id
        }
    }

}