import { CommonModule } from '@angular/common';
import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { GoogleAuthGuard } from '../auth/google.guard';
import { MaterialModule } from '../material.module';
import { TopicPopUpComponent } from '../pop-up/topic-pop-up/topic-pop-up.component';
import { TopicsComponent } from './topics.component';

const routes: Routes = [
  {path: '', component: TopicsComponent, canActivate: [GoogleAuthGuard]}  
] 

@NgModule({
  declarations: [TopicsComponent],
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    MaterialModule
  ],
  exports: [  ],
  entryComponents: [ TopicPopUpComponent ],
  schemas: [ CUSTOM_ELEMENTS_SCHEMA]
})
export class TopicsModule { }
