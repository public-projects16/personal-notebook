import { Injectable } from '@angular/core';
import firebase from "firebase";

import { errorMessages } from 'src/app/utils/error.messages';
import { FirebaseCollections } from 'src/app/utils/firebase.collections';
import { SubjectsService } from '../subject/subjects.service';
import { Topic } from './topic';

@Injectable({
  providedIn: 'root'
})
export class TopicsService {

  private firestore: firebase.firestore.Firestore;

  constructor(
    private subjectService: SubjectsService
  ) {
    this.firestore = firebase.firestore();
   }

  public getTopics(): Promise<Topic[]> {
      let subjectsPromise: Promise<Topic[]>;
      subjectsPromise = new Promise((resolve, reject) => {
          const topicsFound: Topic[] = [];
          let currentSubjectId: string = this.subjectService.currentSubject.id
          this.firestore.collection(FirebaseCollections.topic)
          .where("subjectId", "==", currentSubjectId).get()
          .then((querySnapshot: firebase.firestore.QuerySnapshot<firebase.firestore.DocumentData>) => {
              for (const doc of querySnapshot.docs) {
                const topicFound: Topic = new Topic();
                topicFound.load(doc.data());
                topicFound.id = doc.id;
                topicsFound.push(topicFound);
              }
              resolve(topicsFound);
          })
          .catch(() => {
              reject(new Error().message = errorMessages.topic.errorRetreivingData);
          })
      });
      return subjectsPromise;
  }

}
