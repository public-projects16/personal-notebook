import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ContentComponent } from './content.component';
import { RouterModule, Routes } from '@angular/router';
import { GoogleAuthGuard } from '../auth/google.guard';
import { AngularEditorModule } from '@kolkov/angular-editor';
import { HttpClientModule } from '@angular/common/http';
import { RichTextEditorAllModule } from '@syncfusion/ej2-angular-richtexteditor';
import { MaterialModule } from '../material.module';
import { ToolbarService, LinkService, ImageService, HtmlEditorService } from '@syncfusion/ej2-angular-richtexteditor';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { SafeHtmlPipe } from '../utils/http-pip';

const routes: Routes = [
  { path: '', component: ContentComponent, canActivate: [GoogleAuthGuard] }
]

@NgModule({
  declarations: [ContentComponent, SafeHtmlPipe],
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    RichTextEditorAllModule,
    HttpClientModule,
    MaterialModule,
    ReactiveFormsModule,
    FormsModule
  ],
  providers: [ToolbarService, LinkService, ImageService, HtmlEditorService],
  schemas: [ CUSTOM_ELEMENTS_SCHEMA]
})
export class ContentModule { }
