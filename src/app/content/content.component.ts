import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup } from '@angular/forms';
import { ActivatedRoute, Params } from '@angular/router';
import * as _ from "lodash";

import { Content } from './content';
import { ContentService } from './content.service';
import { SafeHtmlPipe } from '../utils/http-pip';

@Component({
  selector: 'app-content',
  templateUrl: './content.component.html',
  styleUrls: ['./content.component.css']
})
export class ContentComponent implements OnInit {

  public currentContentId: string;
  public content: Content;
  public isNewContent: boolean;
  public config: object;
  public contentForm: FormGroup;

  constructor(
    private contentService: ContentService,
    private route: ActivatedRoute,
    private formBuilder: FormBuilder
  ) {
    this.currentContentId = '';
    this.content = new Content();
    this.isNewContent = false;
    this.contentForm = this.formBuilder.group({});
    this.config = this.getConfig();
   }

  async ngOnInit(): Promise<void> {
    this.setQueryParams();
    await this.setContent();
  }

  public initializeForm(summary?: string) {
    this.contentForm = this.formBuilder.group({
      summary: new FormControl(summary ? summary : '')
    });
  }

  public setQueryParams(): void {
    this.route.queryParams.subscribe((params: Params) => {
      this.currentContentId = params['id'];
    });
  }

  public async setContent(): Promise<void> {
    try {
      this.content = await this.contentService.getContent(this.currentContentId);
      this.isNewContent = _.isEmpty(this.content);
      this.initializeForm(this.content.summary);
    } catch(error) {
      this.isNewContent = true;
      this.initializeForm();
    }
  }

  public async saveContent() {
    const contentToSave = new Content();
    contentToSave.topicId = this.currentContentId;
    contentToSave.summary = this.contentForm.get('summary')?.value;
    contentToSave.id = this.content.id;
    await this.contentService.createContent(contentToSave);
    await this.setContent();
    this.content.summary = contentToSave.summary;
    this.isNewContent = false;
  }

  public editSummary(): void {
    this.contentForm.patchValue({summary: this.content.summary});
    this.isNewContent = true;
  }

  public getConfig(): Object {
    return  {
      type: 'Expand',
      items: ['Bold', 'Italic', 'Underline', 'StrikeThrough',
        'FontName', 'FontSize', 'FontColor', 'BackgroundColor',
        'LowerCase', 'UpperCase', '|',
        'Formats', 'Alignments', 'OrderedList', 'UnorderedList',
        'Outdent', 'Indent', '|',
        'CreateLink', 'Image', '|', 'ClearFormat', 'Print',
        'SourceCode', 'FullScreen', '|', 'Undo', 'Redo']
      };
  }
}
