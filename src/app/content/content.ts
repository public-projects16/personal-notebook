import * as _ from "lodash";

export class Content {

    private _summary: string;
    private _topicId: string;
    private _id: string;

    constructor() {
        this._summary = '';
        this._topicId = '';
        this._id = '';
    }

    get summary(): string { return this._summary; }

    set summary(value: string) { this._summary =  value; }

    get topicId(): string { return this._topicId; }

    set topicId(value: string) { this._topicId = value; }

    get id(): string { return this._id; }

    set id(value: string) { this._id = value; }

    public load(data: any, prefix: string = "") {
        for (let parameter in this) {
            if (!_.isUndefined(typeof data[prefix + _.camelCase(parameter)]) && !_.isEqual(parameter, "constructor")) {
                this[parameter] = data[prefix + _.camelCase(parameter)];
            }
        }
    }

    public toJSON() {
        return {
            id: this.id,
            topicId: this.topicId,
            summary: this.summary
        }
    }
}