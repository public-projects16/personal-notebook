import { Injectable } from '@angular/core';
import firebase from "firebase";
import { FirebaseCollections } from 'src/app/utils/firebase.collections';
import * as _ from "lodash";
import { Content } from './content';
import { errorMessages } from '../utils/error.messages';

@Injectable({
  providedIn: 'root'
})
export class ContentService {

  private firestore: firebase.firestore.Firestore;

  constructor() { 
    this.firestore = firebase.firestore();
  }

  public getContent(topicId: string): Promise<Content> {
    let promise: Promise<Content>;
    promise = new Promise((resolve, reject) => {
      const collection = this.firestore.collection(FirebaseCollections.content);
      collection.where('topicId', '==', topicId).get()
      .then((querySnapshot: firebase.firestore.QuerySnapshot<firebase.firestore.DocumentData>) => {
        const foundDocument = _.head(querySnapshot.docs);
        const contentFound = new Content();
        contentFound.load(foundDocument?.data());
        contentFound.id = foundDocument?.id as string;
        resolve(contentFound);
      })
      .catch(() => {
        reject(errorMessages.content.errorRetreivingData);
      })
    });
    return promise;
  }

  public createContent(content: Content): Promise<void> {
    let promise: Promise<void>;
    promise = new Promise((resolve, reject) => {
      const method = _.isEmpty(content.id) ? this.firestore.collection(FirebaseCollections.content).doc().set(content.toJSON()) :
      this.firestore.collection(FirebaseCollections.content).doc(content.id).update(content.toJSON())
        method
        .then(() => {
            resolve();
        })
        .catch((error: Error) => {
            reject(error);
        });
    });
    return promise;
  }
}
