import { BrowserModule } from '@angular/platform-browser';
import { CUSTOM_ELEMENTS_SCHEMA, NgModule, NO_ERRORS_SCHEMA } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';

import { APP_ROUTES } from './app.routes';
import { AppComponent } from './app.component';
import { AppFirebaseModule } from './app.firebase.module';
import { GoogleAuthGuard } from './auth/google.guard';
import { LoginComponent } from './login/login.component';
import { MaterialModule } from './material.module';
import { NavComponent } from './nav/nav.component';
import { SubjectComponent } from './subject/subject.component';
import { SubjectPopUpComponent } from './pop-up/subject-pop-up/subject-pop-up.component';
import { SubjectsService } from 'src/app/subject/subjects.service';
import { ToastrModule } from 'ngx-toastr';
import { TopicPopUpComponent } from './pop-up/topic-pop-up/topic-pop-up.component';
import { UserService } from './home/user.service';

@NgModule({
  declarations: [
    AppComponent,
    SubjectComponent,
    SubjectPopUpComponent,
    TopicPopUpComponent,
    NavComponent,
    LoginComponent
  ],
  imports: [
    APP_ROUTES,
    BrowserModule,
    MaterialModule,
    BrowserAnimationsModule,
    AppFirebaseModule,
    ToastrModule.forRoot(),
    FormsModule,
    ReactiveFormsModule,
  ],
  providers: [GoogleAuthGuard, SubjectsService, UserService ],
  bootstrap: [AppComponent],
  schemas: [CUSTOM_ELEMENTS_SCHEMA, NO_ERRORS_SCHEMA],
  entryComponents: [SubjectPopUpComponent, TopicPopUpComponent],
  exports: [ MaterialModule ]
})
export class AppModule { }
