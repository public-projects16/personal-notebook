import { catchError, take } from 'rxjs/operators';
import { Component, OnInit } from '@angular/core';
import { EMPTY } from 'rxjs';
import { Router } from '@angular/router';

import { AuthService } from '../auth/auth.service';
import { environment } from 'src/environments/environment';
import { NotificationsService } from '../services/notification.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  
  constructor(
    private readonly auth: AuthService,
    private readonly snackBar: NotificationsService,
    private readonly router: Router,
  ) { }

  ngOnInit(): void {
    
  }

  login() {
    this.auth.loginViaGoogle().pipe(
        take(1),
        catchError((error) => {
          this.snackBar.error("Inicio de sesión inválido")
          return EMPTY;
        }),
      )
      .subscribe((response) => {
        localStorage.setItem(environment.tokenKey, JSON.stringify(response));
        localStorage.setItem(environment.userData, JSON.stringify(response.user));
        this.snackBar.success("Sesión iniciada");
        this.router.navigate(['subjects']);
      });
  }

  logout() {
    this.auth
      .logout()
      .pipe(take(1))
      .subscribe((response) => {
        this.router.navigate([`login`]);
        this.snackBar.info('Sesión finalizada');
      });
  }
}
