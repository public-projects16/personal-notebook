import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';

import { LoginComponent } from './login.component';
import { MaterialModule } from '../material.module';
import { NotificationsService } from '../services/notification.service';



@NgModule({
  declarations: [LoginComponent],
  imports: [
    CommonModule,
    MaterialModule
  ],
providers: [NotificationsService]  
})
export class LoginModule { }
