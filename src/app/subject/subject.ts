import * as _ from "lodash";

export class Subject {

    private _name: string;
    private _id: string;
    private _userId: string;

    constructor() {
        this._name = '';
        this._id = '';
        this._userId =  '';
    }

    get name(): string { return this._name; }

    set name(value: string) { this._name = value; }

    get id(): string { return this._id; }

    set id(value: string) { this._id = value; }

    get userId(): string { return this._userId; }

    set userId(value: string) { this._userId = value; }

    public load(data: any, prefix: string = "") {
        for (let parameter in this) {
            if (!_.isUndefined(typeof data[prefix + _.camelCase(parameter)]) && !_.isEqual(parameter, "constructor")) {
                this[parameter] = data[prefix + _.camelCase(parameter)];
            }
        }
    }

    public toJSON() {
        return {
            id: this.id,
            name: this.name,
            userId: this.userId
        }
    }
}