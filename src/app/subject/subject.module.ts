import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { GoogleAuthGuard } from '../auth/google.guard';
import { MaterialModule } from '../material.module';
import { SubjectComponent } from './subject.component';

const router: Routes = [
  {path: '', component: SubjectComponent, canActivate: [GoogleAuthGuard]}
]

@NgModule({
  declarations: [SubjectComponent],
  imports: [
    CommonModule,
    RouterModule.forChild(router),
    MaterialModule
  ]
})
export class SubjectModule { }
