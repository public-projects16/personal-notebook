import { MatDialogConfig } from "@angular/material/dialog";

export class SubjectPopUpConfiguration {

    public static getPopUpConfiguration(title: string, method: string): MatDialogConfig {
        const subjecDialogConfiguration  = new MatDialogConfig();
        subjecDialogConfiguration.disableClose = true;
        subjecDialogConfiguration.autoFocus = true;
        subjecDialogConfiguration.width = '65%';
        subjecDialogConfiguration.panelClass = 'custom-dialog-container';
        subjecDialogConfiguration.data = {
        title: title,
        method: method,
      };
      return subjecDialogConfiguration;
    } 
}