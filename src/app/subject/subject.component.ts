import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { Router } from '@angular/router';
import * as _ from 'lodash';

import { AuthService } from '../auth/auth.service';
import { errorMessages } from 'src/app/utils/error.messages';
import { NotificationsService } from '../services/notification.service';
import { Subject } from 'src/app/subject/subject';
import { SubjectPopUpComponent } from '../pop-up/subject-pop-up/subject-pop-up.component';
import { SubjectPopUpConfiguration } from './pop-up-subject.config';
import { SubjectsService } from 'src/app/subject/subjects.service';
import { User } from '../home/user';
import { UserService } from '../home/user.service';

@Component({
  selector: 'app-subject',
  templateUrl: './subject.component.html',
  styleUrls: ['./subject.component.css']
})
export class SubjectComponent implements OnInit {

  public subjects: Subject[];
  public name: string;
  public firebaseUser: any;

  constructor(private router: Router, private subjectService: SubjectsService, private authService: AuthService,
      private userService: UserService, private notification: NotificationsService, private popUpDialog: MatDialog) {
    this.subjects = [];
    this.name = '';
   }

  async ngOnInit(): Promise<void> {
    await this.validateUserService();
    await this.setSubjects();
    this.getUserName();
  }

  public async setSubjects() {
    this.subjects = await this.subjectService.getSubjects();
  }

  public goToTopics(selectedSubject: Subject): void {
    this.subjectService.currentSubject = selectedSubject;
    this.router.navigate(['topics']);
  }

  public getUserName() {
    this.name = this.authService.getUserData().displayName || '';
  }

  public async validateUserService() {
    try {
      this.firebaseUser = this.authService.getUserData();
      let userFound: User | undefined = _.head(await this.userService.getUserByEmail(this.firebaseUser.email || ''));
      
      if (!userFound) {
        const newUser: User = new User();
        newUser.email = this.firebaseUser.email || '';
        newUser.name = this.firebaseUser.displayName || '';
        await this.userService.createUser(newUser);
        userFound =  _.head(await this.userService.getUserByEmail(this.firebaseUser.email || ''));
      }

      this.authService.setUserId(userFound?.id || '');

    } catch(error) {
      this.notification.error(errorMessages.user.errorCreating);
      this.router.navigate(['login']);
    }
  }

  public createNewSubject(): void {
    const subjectPopUpConfiguration = SubjectPopUpConfiguration.getPopUpConfiguration('Agregar materia', 'add');
    this.popUpDialog.open(SubjectPopUpComponent, subjectPopUpConfiguration)
    .afterClosed().toPromise().then(() => { this.setSubjects(); });
  }
}
