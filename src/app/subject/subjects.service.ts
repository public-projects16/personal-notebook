import { Injectable } from "@angular/core";
import * as _ from 'lodash';
import firebase from "firebase";

import { AuthService } from "../auth/auth.service";
import { errorMessages } from "../utils/error.messages";
import { FirebaseCollections } from "../utils/firebase.collections";
import { Subject } from "./subject";

@Injectable()
export class SubjectsService {

    private firestore: firebase.firestore.Firestore;

    constructor(private authService: AuthService) {
        this.firestore = firebase.firestore();
    }

    public getSubjects(): Promise<Subject[]> {
        let subjectsPromise: Promise<Subject[]>;
        subjectsPromise = new Promise((resolve, reject) => {
            const subjectsFound: Subject[] = [];
            let currentUserId: string = this.authService.getUserId();
            this.firestore.collection(FirebaseCollections.subject)
            .where("userId", "==", currentUserId).get()
            .then((querySnapshot: firebase.firestore.QuerySnapshot<firebase.firestore.DocumentData>) => {
                for(const doc of querySnapshot.docs) {
                    const subjectFound: Subject = new Subject();
                    subjectFound.load(doc.data());
                    subjectFound.id = doc.id;
                    subjectsFound.push(subjectFound);
                }
                resolve(subjectsFound);
            })
            .catch(() => {
                reject(new Error().message = errorMessages.subject.errorRetreivingData);
            })
        });
        return subjectsPromise;
    }

    get currentSubject(): Subject {
        const subjectStringify = localStorage.getItem("currentSubject") || '';
        return JSON.parse(subjectStringify);
    }

    set currentSubject(subject: Subject) {
        const stringifySubject = JSON.stringify(subject);
        localStorage.setItem("currentSubject", stringifySubject);
    }

}