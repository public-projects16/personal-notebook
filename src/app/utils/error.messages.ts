export const errorMessages = {
    subject: {
        errorRetreivingData: 'Ocurrio un error al obtener las materias'
    },
    user: {
        errorCreating: 'Error al crear al usuario'
    },
    topic: {
        errorRetreivingData: 'Ocurrio un error al obtener los temas'
    },
    content: {
        errorRetreivingData: 'Ocurrio un error al obtener el contenido'
    }
}